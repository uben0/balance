use crate::*;

#[test]
fn test_money_display() {
    let m = [
        //                       "{}"     "{:+}"   "|{: >10}|"
        (Money::from_cents(  0), "0.00" , "+0.00", "      0.00"),
        (Money::from_cents( -0), "0.00" , "+0.00", "      0.00"),
        (Money::from_cents( -1), "-0.01", "-0.01", "     -0.01"),
        (Money::from_cents(  2), "0.02" , "+0.02", "      0.02"),
        (Money::from_cents(201), "2.01" , "+2.01", "      2.01"),
    ];
    for &(a, s0, s1, s2) in &m {
        assert_eq!(&format!("{}"     , a), s0);
        assert_eq!(&format!("{:+}"   , a), s1);
        assert_eq!(&format!("{: >10}", a), s2);
    }
}