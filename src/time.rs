// use std::str::FromStr;
use std::fmt;
use std::io::Write;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy, PartialOrd, Ord, PartialEq, Eq, Hash, Deserialize, Serialize)]
pub struct Time(i64);

impl Time {
    pub fn now() -> Self {
        use std::time::{SystemTime, UNIX_EPOCH};
        Self(SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs() as i64)
    }
}

impl fmt::Display for Time {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        let mut buf = [0u8; 32];
        let size = {
            let mut cursor = &mut buf[..];
            use chrono::prelude::*;
            let date = Local.timestamp(self.0, 0).date();
            write!(cursor, "{}/{:02}/{:02}", date.year(), date.month(), date.day()).unwrap();
            32 - cursor.len()
        };
        let s = std::str::from_utf8(&buf[..size]).unwrap();
        formatter.pad(s)
    }
}