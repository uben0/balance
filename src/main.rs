use serde::{Deserialize, Serialize};
use std::fs::File;
use structopt::StructOpt;

mod term;

mod money;
use money::Money;

mod time;
use time::Time;

#[derive(Deserialize, Default)]
struct Config {
    main_balance_path: Option<String>,
}
impl Config {
    fn load() -> Result<Self, Box<dyn std::error::Error>> {
        Ok(if let Ok(mut path) = std::env::var("HOME") {
            path += "/.config/balance/config.toml";
            if let Ok(mut file) = std::fs::File::open(path) {
                use std::io::Read;
                let mut buffer = Vec::new();
                file.read_to_end(&mut buffer)?;
                toml::from_slice(&buffer)?
            } else {
                Self::default()
            }
        } else {
            Self::default()
        })
    }
}

#[derive(Debug, Deserialize, Serialize)]
struct Action {
    amount: Money,
    description: String,
}
impl Action {
    fn new(amount: Money, description: String) -> Self {
        Self {
            amount,
            description,
        }
    }
    fn confirm_now(self) -> ConfirmedAction {
        self.confirm(Time::now())
    }
    fn confirm(self, date: Time) -> ConfirmedAction {
        let Self{amount, description} = self;
        ConfirmedAction::new(amount, description, date)
    }
}

#[derive(Debug, Deserialize, Serialize)]
struct ConfirmedAction {
    amount: Money,
    description: String,
    date: Time,
}
impl ConfirmedAction {
    fn new(amount: Money, description: String, date: Time) -> Self {
        Self{
            amount,
            description,
            date,
        }
    }
    fn revoke(self) -> Action {
        let Self{amount, description, ..} = self;
        Action{
            amount,
            description,
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
struct Balance {
    balance: Money,
    pending: Vec<Action>,
    confirmed: Vec<ConfirmedAction>,
}
impl Balance {
    fn new(balance: Money) -> Self {
        Self {
            balance,
            confirmed: Vec::new(),
            pending: Vec::new(),
        }
    }
    fn action_in(&mut self, amount: Money, description: String, confirmed: bool) {
        let action = Action::new(amount, description);
        if confirmed {
            self.balance = self.balance + amount;
            self.confirmed.push(action.confirm_now());
        } else {
            self.pending.push(action);
        }
    }
    fn action_out(&mut self, amount: Money, description: String, confirmed: bool) {
        let action = Action::new(-amount, description);
        if confirmed {
            self.balance = self.balance - amount;
            self.confirmed.push(action.confirm_now());
        } else {
            self.pending.push(action);
        }
    }
    fn confirm_pending(&mut self, index: usize) {
        let action = self.pending.remove(index);
        self.balance = self.balance + action.amount;
        self.confirmed.push(action.confirm_now());
    }
    fn revoke_confirmed(&mut self, index: usize) {
        let index = self.confirmed.len() - 1 - index;
        let action = self.confirmed.remove(index);
        self.balance = self.balance - action.amount;
        self.pending.push(action.revoke());
    }
    fn cancel_pending(&mut self, index: usize) {
        self.pending.remove(index);
    }
    fn load_from_file(path: &str) -> Result<Self, Box<dyn std::error::Error>> {
        Ok(serde_json::from_reader(File::open(path)?)?)
    }
    fn save_to_file(&self, path: &str) -> Result<(), Box<dyn std::error::Error>> {
        Ok(serde_json::to_writer_pretty(File::create(path)?, self)?)
    }
    fn compute_confirmed_bal(&self) -> Money {
        self.balance
    }
    fn compute_expected_bal(&self) -> Money {
        self.pending
            .iter()
            .fold(self.compute_confirmed_bal(), |b, m| b + m.amount)
    }
    fn print_status(&self, limit: usize) {
        if !self.pending.is_empty() {
            println!(
                "pending    {}{}{: >10}{}",
                term::BOLD,
                term::DIM,
                self.compute_expected_bal(),
                term::RESET
            );
            for (index, pending) in self.pending.iter().enumerate() {
                let color = if pending.amount.is_negative() {
                    term::LIGHT_RED
                } else {
                    term::LIGHT_GREEN
                };
                println!(
                    "{: >2}         {}{}{: >+10}{}  {}",
                    index,
                    color,
                    term::DIM,
                    pending.amount,
                    term::RESET,
                    pending.description
                );
            }
            println!();
        }
        println!(
            "           {}{: >10}{}",
            term::BOLD,
            self.compute_confirmed_bal(),
            term::RESET
        );
        for action in self.confirmed.iter().rev().take(limit) {
            let color = if action.amount.is_negative() {
                term::LIGHT_RED
            } else {
                term::LIGHT_GREEN
            };
            println!(
                "{} {}{: >+10}{}  {}",
                action.date,
                color,
                action.amount,
                term::FG_RESET,
                action.description
            );
        }
    }
}

#[derive(Debug, StructOpt)]
struct Opt {
    #[structopt(short, long)]
    file: Option<String>,
    #[structopt(short, long, default_value = "10")]
    limit: usize,
    #[structopt(subcommand)]
    cmd: Option<Command>,
}

#[derive(Debug, StructOpt)]
enum Command {
    /// Show the current balance and all actions
    Status,
    /// Initialize a new balance
    New {
        #[structopt(default_value = "0")]
        base: Money,
    },
    /// Add an income action
    In {
        amount: Money,
        description: String,
        #[structopt(short, long)]
        confirmed: bool,
    },
    /// Add an outcome action
    Out {
        amount: Money,
        description: String,
        #[structopt(short, long)]
        confirmed: bool,
    },
    /// Confirm a pending action
    Confirm { number: usize },
    /// Cancel a pending action
    Cancel { number: usize },
    /// Undo confirmation of an action
    Revoke { #[structopt(default_value="0")] number: usize },
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let Config { main_balance_path } = Config::load()?;
    let Opt { file, cmd, limit } = Opt::from_args();
    let file = file.unwrap_or_else(|| main_balance_path.unwrap_or_else(|| "balance.json".into()));
    match cmd.unwrap_or(Command::Status) {
        Command::New { base } => {
            let balance = Balance::new(base);
            balance.save_to_file(&file)?;
        }
        Command::Status => {
            let balance = Balance::load_from_file(&file)?;
            balance.print_status(limit);
        }
        Command::In {
            amount,
            description,
            confirmed,
        } => {
            let mut balance = Balance::load_from_file(&file)?;
            balance.action_in(amount, description, confirmed);
            balance.save_to_file(&file)?;
            balance.print_status(limit);
        }
        Command::Out {
            amount,
            description,
            confirmed,
        } => {
            let mut balance = Balance::load_from_file(&file)?;
            balance.action_out(amount, description, confirmed);
            balance.save_to_file(&file)?;
            balance.print_status(limit);
        }
        Command::Confirm { number } => {
            let mut balance = Balance::load_from_file(&file)?;
            if number >= balance.pending.len() {
                return Err("index out of range".into());
            }
            balance.confirm_pending(number);
            balance.save_to_file(&file)?;
            balance.print_status(limit);
        }
        Command::Cancel { number } => {
            let mut balance = Balance::load_from_file(&file)?;
            if number >= balance.pending.len() {
                return Err("index out of range".into());
            }
            balance.cancel_pending(number);
            balance.save_to_file(&file)?;
            balance.print_status(limit);
        }
        Command::Revoke { number } => {
            let mut balance = Balance::load_from_file(&file)?;
            if number >= balance.confirmed.len() {
                return Err("index out of range".into());
            }
            balance.revoke_confirmed(number);
            balance.save_to_file(&file)?;
            balance.print_status(limit);
        }
    }
    Ok(())
}

#[cfg(test)]
mod test;
