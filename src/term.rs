pub const BOLD       : &str = "\x1b[1m";
pub const DIM        : &str = "\x1b[2m";
pub const RESET      : &str = "\x1b[0m";
// pub const RED        : &str = "\x1b[31m";
// pub const GREEN      : &str = "\x1b[32m";
pub const LIGHT_RED  : &str = "\x1b[91m";
pub const LIGHT_GREEN: &str = "\x1b[92m";
pub const FG_RESET   : &str = "\x1b[39m";