use std::str::FromStr;
use std::fmt;
use std::io::Write;
use std::ops::{Add, Sub, Neg};
use serde::{Deserialize, Serialize};

pub const ZERO: Money = Money(0);

#[derive(Debug)]
pub struct ParseMoneyError;
impl fmt::Display for ParseMoneyError {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        "ParseMoneyError".fmt(formatter)
    }
}

#[derive(Debug, Clone, Copy, PartialOrd, Ord, PartialEq, Eq, Hash, Deserialize, Serialize)]
pub struct Money(i64);
impl Money {
    pub const fn from_cents(cents: i64) -> Self {
        Self(cents)
    }
    pub fn is_negative(self) -> bool {
        self < ZERO
    }
}
impl fmt::Display for Money {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        let mut buf = [0u8; 32];
        let size = {
            let mut cursor = &mut buf[..];
            let val = self.0.abs();
            write!(cursor, "{}.{:02}", val / 100, val % 100).unwrap();
            32 - cursor.len()
        };
        let s = std::str::from_utf8(&buf[..size]).unwrap();
        formatter.pad_integral(self.0 >= 0, "", s)
    }
}
impl Add for Money {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        Money(self.0 + rhs.0)
    }
}
impl Sub for Money {
    type Output = Self;
    fn sub(self, rhs: Self) -> Self::Output {
        Money(self.0 - rhs.0)
    }
}
impl Neg for Money {
    type Output = Self;
    fn neg(self) -> Self::Output {
        Money(-self.0)
    }
}
impl FromStr for Money {
    type Err = ParseMoneyError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        fn char_to_n(c: char) -> Option<u64> {
            Some(match c {
                '0' => 0, '1' => 1,
                '2' => 2, '3' => 3,
                '4' => 4, '5' => 5,
                '6' => 6, '7' => 7,
                '8' => 8, '9' => 9,
                _ => return None,
            })
        }

        let mut iter = s.chars().peekable();
        let negative = match iter.peek() {
            Some('+') => {iter.next(); false}
            Some('-') => {iter.next(); true}
            Some( _ ) => {false}
            None => {return Err(ParseMoneyError)}
        };
        let mut plain_val: u64 = 0;
        while let Some('0'..='9') = iter.peek() {
            plain_val *= 10;
            plain_val += char_to_n(iter.next().unwrap()).unwrap();
        }
        let cents_val: u64 = if let Some(c) = iter.next() {
            if c != '.' {
                return Err(ParseMoneyError);
            }
            match (iter.next(), iter.next()) {
                (Some(c1), Some(c2)) => {
                    char_to_n(c1).ok_or(ParseMoneyError)? * 10 +
                    char_to_n(c2).ok_or(ParseMoneyError)?
                }
                _ => {return Err(ParseMoneyError);}
            }
        } else {0};
        let val = (plain_val * 100 + cents_val) as i64;
        Ok(Money::from_cents(if negative {-val} else {val}))
    }
}