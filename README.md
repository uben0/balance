# Balance - Keep in touch with your finance

```
USAGE:
    balance [OPTIONS] [SUBCOMMAND]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -f, --file <file>      
    -l, --limit <limit>     [default: 10]

SUBCOMMANDS:
    cancel     Cancel a pending action
    confirm    Confirm a pending action
    help       Prints this message or the help of the given subcommand(s)
    in         Add an income action
    new        Initialize a new balance
    out        Add an outcome action
    revoke     Undo confirmation of an action
    status     Show the current balance and all actions
```
